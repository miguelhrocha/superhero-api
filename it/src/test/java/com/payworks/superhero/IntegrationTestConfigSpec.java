package com.payworks.superhero;

public interface IntegrationTestConfigSpec {

    int port();
    String hostname();

}
