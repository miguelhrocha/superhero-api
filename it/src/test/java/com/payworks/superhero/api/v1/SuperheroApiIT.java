package com.payworks.superhero.api.v1;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.payworks.superhero.IntegrationTestGuiceModule;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.NotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class SuperheroApiIT {

    private Injector injector = Guice.createInjector(new IntegrationTestGuiceModule());

    private EnhancedRandom enhancedRandom = injector.getInstance(EnhancedRandom.class);
    private SuperheroApi superheroApi = injector.getInstance(SuperheroApi.class);

    @Nested
    class createSuperhero {

        @Test
        @DisplayName("Asserts that a superhero can be created without allies")
        public void canCreateSuperheroWithoutAllies() {
            CreateSuperheroResponse createdSuperhero = createSuperheroWithoutAllies();

            assertNotNull(createdSuperhero);
            assertFalse(createdSuperhero.getId().isEmpty());
            assertEquals(createdSuperhero.getName(), createdSuperhero.getName());
        }

        @Test
        @DisplayName("Throws 400 BAD REQUEST when name is empty")
        public void throwsBadRequestWhenNameIsEmpty() {
            CreateSuperheroRequest request = new CreateSuperheroRequest(
                    "",
                    enhancedRandom.nextObject(String.class),
                    "DC",
                    EnhancedRandom.randomListOf(2, String.class),
                    EnhancedRandom.randomListOf(3, AllyTransferObject.class),
                    EnhancedRandom.random(LocalDate.class).toString()
            );

            assertThrows(BadRequestException.class, () -> superheroApi.createSuperhero(request));
        }

        @Test
        @DisplayName("Throws 400 BAD REQUEST when pseudonym is empty")
        public void throwsBadRequestWhenPseudonymIsEmpty() {
            CreateSuperheroRequest request = new CreateSuperheroRequest(
                    enhancedRandom.nextObject(String.class),
                    "",
                    "DC",
                    EnhancedRandom.randomListOf(2, String.class),
                    EnhancedRandom.randomListOf(3, AllyTransferObject.class),
                    EnhancedRandom.random(LocalDate.class).toString()
            );

            assertThrows(BadRequestException.class, () -> superheroApi.createSuperhero(request));
        }

        @ParameterizedTest
        @DisplayName("Throws 400 BAD REQUEST when publisher is neither DC nor MARVEL")
        @ValueSource(strings = {"Dc", "Marvel", "Vertigo", "dc", "marvel"})
        public void throwsBadRequestWithInvalidPublisher(String publisher) {
            CreateSuperheroRequest request = new CreateSuperheroRequest(
                    enhancedRandom.nextObject(String.class),
                    enhancedRandom.nextObject(String.class),
                    publisher,
                    EnhancedRandom.randomListOf(2, String.class),
                    EnhancedRandom.randomListOf(3, AllyTransferObject.class),
                    EnhancedRandom.random(LocalDate.class).toString()
            );

            assertThrows(BadRequestException.class, () -> superheroApi.createSuperhero(request));
        }

        @Test
        @DisplayName("Throws 400 BAD REQUEST when skills are empty")
        public void throwsBadRequestWhenSkillsAreEmpty() {
            CreateSuperheroRequest request = new CreateSuperheroRequest(
                    enhancedRandom.nextObject(String.class),
                    enhancedRandom.nextObject(String.class),
                    "DC",
                    new ArrayList<>(),
                    EnhancedRandom.randomListOf(3, AllyTransferObject.class),
                    EnhancedRandom.random(LocalDate.class).toString()
            );

            assertThrows(BadRequestException.class, () -> superheroApi.createSuperhero(request));
        }

        @Test
        @DisplayName("Throws 400 BAD REQUEST when allies does not exists in database")
        public void throwsBadRequestWhenAlliesDoesNotExist() {
            CreateSuperheroRequest request = new CreateSuperheroRequest(
                    enhancedRandom.nextObject(String.class),
                    enhancedRandom.nextObject(String.class),
                    "DC",
                    EnhancedRandom.randomListOf(2, String.class),
                    EnhancedRandom.randomListOf(3, AllyTransferObject.class),
                    EnhancedRandom.random(LocalDate.class).toString()
            );

            assertThrows(BadRequestException.class, () -> superheroApi.createSuperhero(request));
        }

        @ParameterizedTest
        @DisplayName("Throws 400 BAD REQUEST when appearance date it's not in YYYY-MM-DD format")
        @ValueSource(strings = {"3 Jun 2008 11:05", "Tue, 3 Jun 2008 11:05:30 GMT", "2011-12-03T10:15:30Z", "2011-12-03T10:15:30"})
        public void throwsBadRequestWhenAppearanceDateDoesNotCompliantWithFormat(String date) {
            CreateSuperheroRequest request = new CreateSuperheroRequest(
                    enhancedRandom.nextObject(String.class),
                    enhancedRandom.nextObject(String.class),
                    "DC",
                    EnhancedRandom.randomListOf(2, String.class),
                    null,
                    date
            );

            assertThrows(BadRequestException.class, () -> superheroApi.createSuperhero(request));
        }
    }

    @Nested
    class getSuperhero {

        @Test
        @DisplayName("Returns a superhero previously existing")
        public void canRetrieveExistingSuperhero() {
            CreateSuperheroResponse createSuperheroResponse = createSuperheroWithoutAllies();

            GetSuperheroResponse getSuperheroResponse = superheroApi.getSuperhero(createSuperheroResponse.getId());

            assertNotNull(getSuperheroResponse);
            assertEquals(createSuperheroResponse.getId(), getSuperheroResponse.getId());
            assertEquals(createSuperheroResponse.getId(), getSuperheroResponse.getId());
            assertNotNull(getSuperheroResponse.getName());
        }

        @Test
        @DisplayName("Returns 404 NOT FOUND when superheroId does not exist")
        public void throwsNotFoundExceptionOnNonExistingSuperhero() {
            assertThrows(NotFoundException.class, () -> superheroApi.getSuperhero(UUID.randomUUID().toString()));
        }

        @Test
        @DisplayName("Returns 400 BAD REQUEST when superheroId is not a valid UUID")
        public void throwsBadRequestWhenSuperheroIdIsInvalid() {
            assertThrows(BadRequestException.class, () -> superheroApi.getSuperhero(enhancedRandom.nextObject(String.class)));
        }

    }

    @Nested
    class getSuperheroList {

        @Test
        @DisplayName("Can return first page of super heroes")
        public void canReturnFirstPageOfSuperheroes() {
            createPageOfSuperheroes();

            GetSuperheroListResponse getSuperheroListResponse = superheroApi.getSuperheroList(0);

            assertNotNull(getSuperheroListResponse.getSuperheroes());
            assertFalse(getSuperheroListResponse.getSuperheroes().isEmpty());
            assertEquals(10, getSuperheroListResponse.getSuperheroes().size());
        }

        @Test
        @DisplayName("Can return second page of super heroes after retrieving the first")
        public void canReturnSecondPageOfSuperheroes() {
            createPageOfSuperheroes();
            createPageOfSuperheroes();

            GetSuperheroListResponse firstPageResponse = superheroApi.getSuperheroList(0);
            GetSuperheroListResponse secondPageResponse = superheroApi.getSuperheroList(1);

            assertNotNull(secondPageResponse);
            assertNotEquals(firstPageResponse, secondPageResponse);
            assertEquals(10, secondPageResponse.getSuperheroes().size());
        }

        @Test
        @DisplayName("Throws 400 BAD REQUEST when offset is negative number")
        public void throwsBadRequestWithNegativeOffset() {
            assertThrows(BadRequestException.class, () -> superheroApi.getSuperheroList(-1));
        }

    }

    private CreateSuperheroResponse createSuperheroWithoutAllies() {
        CreateSuperheroRequest request = new CreateSuperheroRequest(
                enhancedRandom.nextObject(String.class),
                enhancedRandom.nextObject(String.class),
                "DC",
                EnhancedRandom.randomListOf(2, String.class),
                null,
                enhancedRandom.nextObject(LocalDate.class).toString()
        );

        return superheroApi.createSuperhero(request);
    }

    private void createPageOfSuperheroes() {
        for (int i = 0; i < 10; i++) {
            createSuperheroWithoutAllies();
        }
    }

}
