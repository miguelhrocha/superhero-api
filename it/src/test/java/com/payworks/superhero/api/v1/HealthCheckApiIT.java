package com.payworks.superhero.api.v1;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.payworks.superhero.IntegrationTestGuiceModule;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HealthCheckApiIT {

    private Injector injector = Guice.createInjector(new IntegrationTestGuiceModule());
    private HealthCheckApi healthCheckApi = injector.getInstance(HealthCheckApi.class);

    @Test
    public void canGetOkStatusCode() {
        assertEquals("OK", healthCheckApi.checkHealth());
    }

}
