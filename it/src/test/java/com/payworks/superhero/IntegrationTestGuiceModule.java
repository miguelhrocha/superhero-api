package com.payworks.superhero;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.base.ProviderBase;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.payworks.superhero.api.v1.HealthCheckApi;
import com.payworks.superhero.api.v1.SuperheroApi;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.ConfigurationSource;
import org.cfg4j.source.classpath.ClasspathConfigurationSource;
import org.cfg4j.source.context.filesprovider.ConfigFilesProvider;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class IntegrationTestGuiceModule extends AbstractModule {

    @Override
    protected void configure() {

        ConfigurationSource source = new ClasspathConfigurationSource(getConfig());
        ConfigurationProvider provider = new ConfigurationProviderBuilder()
                .withConfigurationSource(source)
                .build();

        IntegrationTestConfigSpec configSpec = provider.bind("", IntegrationTestConfigSpec.class);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        JacksonJsonProvider jsonProvider = new JacksonJsonProvider();
        jsonProvider.setMapper(objectMapper);

        bind(ObjectMapper.class).toInstance(objectMapper);
        bind(JacksonJsonProvider.class).toInstance(jsonProvider);

        bind(EnhancedRandom.class).toInstance(EnhancedRandomBuilder.aNewEnhancedRandom());

        bind(IntegrationTestConfigSpec.class).toInstance(configSpec);
    }

    @Provides
    public HealthCheckApi provideHealthCheckApi(
            IntegrationTestConfigSpec configSpec
    ) {
        String apiAddress = String.format(
                "http://%s:%s/api",
                configSpec.hostname(),
                configSpec.port()
        );

        return JAXRSClientFactory.create(
                apiAddress,
                HealthCheckApi.class
        );
    }

    @Provides
    public SuperheroApi provideSuperheroApi(
            IntegrationTestConfigSpec configSpec,
            JacksonJsonProvider jsonProvider
    ) {
        String apiAddress = String.format(
                "http://%s:%s/api",
                configSpec.hostname(),
                configSpec.port()
        );

        List<ProviderBase> providers = new ArrayList<>();
        providers.add(jsonProvider);

        return JAXRSClientFactory.create(
                apiAddress,
                SuperheroApi.class,
                providers
        );
    }

    private ConfigFilesProvider getConfig() {
        String env = System.getenv("app_env");

        String configPathString = "config" +
                (env == null || env.isEmpty() ? "" : String.format(".%s", env)) +
                ".properties";

        return () -> Paths.get(configPathString);
    }

}
