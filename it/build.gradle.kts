plugins {
    java
}

val jacksonVersion = "2.9.6"

dependencies {
    compile(project(":api"))

    testCompile("org.apache.cxf", "cxf-rt-rs-client", "3.2.6")

    testCompile("com.google.inject", "guice", "4.2.0")

    testCompile("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacksonVersion")
    testCompile("com.fasterxml.jackson.core:jackson-core:$jacksonVersion")
    testCompile("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")
    testCompile("com.fasterxml.jackson.jaxrs:jackson-jaxrs-json-provider:$jacksonVersion")

    testCompile("javax.xml.bind", "jaxb-api", "2.3.0")
    testCompile("javax.activation", "activation", "1.1.1")

    testCompile("org.slf4j", "slf4j-api", "1.7.25")
    testCompile("org.slf4j", "slf4j-log4j12", "1.7.25")

    testCompile("org.cfg4j", "cfg4j-core", "4.4.1")

    testCompile("org.junit.jupiter", "junit-jupiter-api", "5.2.0")
    testCompile("org.junit.jupiter", "junit-jupiter-engine", "5.2.0")
    testCompile("org.junit.jupiter","junit-jupiter-params","5.2.0")

    testCompile("io.github.benas", "random-beans", "3.7.0")
}