plugins {
    java
}

dependencies {
    compile(project(":impl"))

    compile("com.google.inject", "guice", "4.2.0")
    compile("org.mongodb", "mongo-java-driver", "3.8.1")

    testCompile("org.cfg4j", "cfg4j-core", "4.4.1")

    testCompile("org.junit.jupiter", "junit-jupiter-api", "5.2.0")
    testCompile("org.junit.jupiter", "junit-jupiter-engine", "5.2.0")

    testCompile("io.github.benas", "random-beans", "3.7.0")
}

