package com.payworks.superhero.mappers;

import com.payworks.superhero.domain.model.Ally;
import com.payworks.superhero.domain.model.SuperheroId;
import org.bson.Document;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class AllyMapperImpl implements AllyMapper {

    @Override
    public Document mapToDocument(Ally ally) {
        return new Document()
                .append("_id", ally.getSuperheroId().getIdString())
                .append("name", ally.getName());
    }

    @Override
    public Ally mapToModel(Document document) {
        SuperheroId superheroId = new SuperheroId(UUID.fromString(document.getString("_id")));
        String allyName = document.getString("name");
        return new Ally(superheroId, allyName);
    }

    @Override
    public List<Document> mapToCollectionOfDocuments(List<Ally> allies) {
        return allies
                .stream()
                .map(this::mapToDocument)
                .collect(Collectors.toList());
    }

    @Override
    public List<Ally> mapToCollectionOfModels(List<Document> documents) {
        return documents
                .stream()
                .map(this::mapToModel)
                .collect(Collectors.toList());
    }
}
