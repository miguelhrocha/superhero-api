package com.payworks.superhero.mappers;

import com.payworks.superhero.domain.model.Ally;
import org.bson.Document;

public interface AllyMapper extends Mapper<Document, Ally> {
}
