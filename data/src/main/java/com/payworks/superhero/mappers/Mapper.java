package com.payworks.superhero.mappers;


import java.util.List;

public interface Mapper<TDocument, TModel> {

    TDocument mapToDocument(TModel model);

    TModel mapToModel(TDocument document);

    List<TDocument> mapToCollectionOfDocuments(List<TModel> models);

    List<TModel> mapToCollectionOfModels(List<TDocument> documents);

}
