package com.payworks.superhero.mappers;

import com.payworks.superhero.application.superhero.PublisherEnum;
import com.payworks.superhero.domain.model.Ally;
import com.payworks.superhero.domain.model.Skill;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import org.bson.Document;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

public class SuperheroMapperImpl implements SuperheroMapper {

    private final AllyMapper allyMapper;

    @Inject
    public SuperheroMapperImpl(AllyMapper allyMapper) {
        this.allyMapper = allyMapper;
    }

    @Override
    public Document mapToDocument(Superhero superhero) {
        List<String> skills = superhero.getSkills()
                .stream()
                .map(Skill::getDescription)
                .collect(Collectors.toList());

        List<Document> allies = allyMapper.mapToCollectionOfDocuments(superhero.getAllies());

        return new Document()
                .append("_id", superhero.getSuperheroId().getIdString())
                .append("name", superhero.getName())
                .append("pseudonym", superhero.getPseudonym())
                .append("publisher", superhero.getPublisher().toString())
                .append("skills", skills)
                .append("allies", allies)
                .append("appearanceDate", superhero.getAppearanceDate().toString());
    }

    @Override
    public Superhero mapToModel(Document document) {
        SuperheroId id = new SuperheroId(UUID.fromString(Objects.requireNonNull(document).getString("_id")));
        String name = document.getString("name");
        String pseudonym = document.getString("pseudonym");
        PublisherEnum publisher = PublisherEnum.valueOf(document.getString("publisher"));

        List<Skill> skills = ((List<String>) document.get("skills", List.class))
                .stream()
                .map(Skill::new)
                .collect(Collectors.toList());

        List<Ally> allies = allyMapper
                .mapToCollectionOfModels((List<Document>) document.get("allies", List.class));

        LocalDate appearanceDate = LocalDate.parse(document.getString("appearanceDate"));

        return new Superhero(
                id,
                name,
                pseudonym,
                publisher,
                skills,
                allies,
                appearanceDate
        );
    }

    @Override
    public List<Document> mapToCollectionOfDocuments(List<Superhero> superheroes) {
        return superheroes
                .stream()
                .map(this::mapToDocument)
                .collect(Collectors.toList());
    }

    @Override
    public List<Superhero> mapToCollectionOfModels(List<Document> documents) {
        return documents
                .stream()
                .map(this::mapToModel)
                .collect(Collectors.toList());
    }
}
