package com.payworks.superhero.mappers;

import com.payworks.superhero.domain.model.Superhero;
import org.bson.Document;

public interface SuperheroMapper extends Mapper<Document, Superhero> {
}
