package com.payworks.superhero;

import com.mongodb.Cursor;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.payworks.superhero.annotations.MongoSuperheroCollection;
import com.payworks.superhero.annotations.MongoSuperheroDatabase;
import com.payworks.superhero.domain.model.Ally;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.services.CreateSuperhero;
import com.payworks.superhero.infrastructure.services.GetSuperhero;
import com.payworks.superhero.infrastructure.services.GetSuperheroList;
import com.payworks.superhero.infrastructure.services.ValidateAllies;
import com.payworks.superhero.mappers.SuperheroMapper;
import org.bson.Document;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.in;
import static org.apache.commons.lang3.Validate.notBlank;

public class SuperheroDatabaseProvider implements CreateSuperhero, ValidateAllies, GetSuperhero, GetSuperheroList {

    private final MongoClient mongoClient;
    private final SuperheroMapper mapper;
    private final String database;
    private final String collection;

    @Inject
    public SuperheroDatabaseProvider(
            MongoClient mongoClient,
            SuperheroMapper mapper,
            @MongoSuperheroDatabase String database,
            @MongoSuperheroCollection String collection
    ) {
        notBlank(database, "Database name should not be null nor blank");
        notBlank(collection, "Collection name should not be null nor blank");

        this.mongoClient = mongoClient;
        this.mapper = mapper;
        this.database = database;
        this.collection = collection;
    }

    @Override
    public Superhero create(Superhero model) {
        MongoDatabase mongoDatabase = mongoClient.getDatabase(database);
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collection);

        Document toCreate = mapper.mapToDocument(model);
        mongoCollection.insertOne(toCreate);

        Document justCreated = mongoCollection.find(eq(
                "_id", model.getSuperheroId().getIdString()
        )).first();

        return mapper.mapToModel(justCreated);
    }

    @Override
    public boolean validate(List<Ally> allies) {
        MongoDatabase mongoDatabase = mongoClient.getDatabase(database);
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collection);

        List<Document> documents = mongoCollection.find(in(
                "_id",
                allies.stream().map(a -> a.getSuperheroId().getIdString()).collect(Collectors.toList()))
        ).into(new ArrayList<>());

        return documents.size() == allies.size();
    }

    @Override
    public Superhero get(SuperheroId superheroId) {
        MongoDatabase mongoDatabase = mongoClient.getDatabase(database);
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collection);

        Document superheroDocument = mongoCollection.find(eq(
                "_id",
                superheroId.getIdString()
        )).first();

        return superheroDocument == null ? null : mapper.mapToModel(superheroDocument);
    }

    @Override
    public List<Superhero> getList(int offset) {
        MongoDatabase mongoDatabase = mongoClient.getDatabase(database);
        MongoCollection<Document> mongoCollection = mongoDatabase.getCollection(collection);

        MongoCursor<Document> documentsCursor = mongoCollection.find().skip(offset * 10).limit(10).iterator();

        List<Superhero> result = new ArrayList<>();

        while (documentsCursor.hasNext()) {
            result.add(mapper.mapToModel(documentsCursor.next()));
        }

        return result;
    }
}
