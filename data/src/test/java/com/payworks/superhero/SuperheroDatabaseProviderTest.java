package com.payworks.superhero;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.payworks.superhero.annotations.MongoSuperheroCollection;
import com.payworks.superhero.annotations.MongoSuperheroDatabase;
import com.payworks.superhero.domain.model.Ally;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.mappers.AllyMapper;
import com.payworks.superhero.mappers.SuperheroMapper;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.bson.Document;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.jupiter.api.Assertions.*;

public class SuperheroDatabaseProviderTest {

    private Injector injector = Guice.createInjector(new MongoGuiceModule());

    private final SuperheroDatabaseProvider superheroDatabaseProvider =
            injector.getInstance(SuperheroDatabaseProvider.class);

    private final String database =
            injector.getInstance(Key.get(String.class, MongoSuperheroDatabase.class));
    private final String collection =
            injector.getInstance(Key.get(String.class, MongoSuperheroCollection.class));
    private final MongoClient mongoClient =
            injector.getInstance(MongoClient.class);
    private final AllyMapper allyMapper =
            injector.getInstance(AllyMapper.class);
    private final SuperheroMapper superheroMapper =
            injector.getInstance(SuperheroMapper.class);

    @Nested
    class constructor {

        @Test
        @DisplayName("Throws InvalidArgumentException when database name parameter is null or blank")
        public void throwsIllegalArgumentExceptionWhenDatabaseNameIsNullOrBlank() {
            String expectedMessage = "Database name should not be null nor blank";

            Throwable throwable = assertThrows(
                    IllegalArgumentException.class,
                    () -> new SuperheroDatabaseProvider(
                            mongoClient,
                            superheroMapper,
                            "",
                            EnhancedRandom.random(String.class)
                    )
            );

            assertEquals(expectedMessage, throwable.getMessage());
        }

        @Test
        @DisplayName("Throws InvalidArgumentException when mongo collection name parameter is null or blank")
        public void throwsIllegalArgumentExceptionWhenCollectionNameIsNullOrBlank() {
            String expectedMessage = "Collection name should not be null nor blank";

            Throwable throwable = assertThrows(
                    IllegalArgumentException.class,
                    () -> new SuperheroDatabaseProvider(
                            mongoClient,
                            superheroMapper,
                            database,
                            ""
                    )
            );

            assertEquals(expectedMessage, throwable.getMessage());
        }

    }

    @Nested
    class create {

        @Test
        @DisplayName("Asserts that the database provider can create a super hero")
        void canCreateSuperhero() {
            Superhero superhero = EnhancedRandom.random(Superhero.class);
            Superhero createdSuperhero = superheroDatabaseProvider.create(superhero);

            MongoDatabase superheroDatabase = mongoClient.getDatabase(database);
            MongoCollection<Document> superheroCollection = superheroDatabase.getCollection(collection);

            Document existingSuperhero = superheroCollection.find(
                    eq("_id", createdSuperhero.getSuperheroId().getIdString())
            ).first();

            assertNotNull(existingSuperhero);
            assertEquals(superhero.getSuperheroId().getIdString(), existingSuperhero.getString("_id"));
        }

    }

    @Nested
    class validate {

        @Test
        @DisplayName("Returns true if every one of the allies list exists in the database")
        public void returnsTrueIfAllAlliesExistsInCollection() {
            MongoDatabase superheroDatabase = mongoClient.getDatabase(database);
            MongoCollection<Document> superheroCollection = superheroDatabase.getCollection(collection);

            MongoCursor<Document> cursor = superheroCollection.find().limit(5).iterator();

            List<Ally> allies = new ArrayList<>();

            while (cursor.hasNext()) {
                allies.add(allyMapper.mapToModel(cursor.next()));
            }

            boolean validAllies = superheroDatabaseProvider.validate(allies);
            assertTrue(validAllies);
        }

        @Test
        @DisplayName("Returns false if at least one of the allies in the list does not exists in the database")
        public void returnsFalseIfAnyAllyDoesNotExistInCollection() {
            List<Ally> allies = EnhancedRandom.randomListOf(5, Ally.class);

            boolean validAllies = superheroDatabaseProvider.validate(allies);
            assertFalse(validAllies);
        }

    }

    @Nested
    class get {

        @Test
        @DisplayName("Can return previously existing superhero")
        public void returnsCreatedSuperhero() {
            Superhero superhero = EnhancedRandom.random(Superhero.class);
            Superhero expected = superheroDatabaseProvider.create(superhero);

            Superhero actual = superheroDatabaseProvider.get(superhero.getSuperheroId());

            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Returns null if superhero wasn't found")
        public void returnsNullIfSuperheroDoesNotExist() {
            assertNull(superheroDatabaseProvider.get(new SuperheroId(UUID.randomUUID())));
        }

    }

    @Nested
    class getList {

        @Test
        @DisplayName("Can get first page (10 results) with default offset value")
        public void canGetFirstPage() {
            List<Superhero> superheroes = new ArrayList<>();
            for (int i = 0; i < 11; i++) {
                superheroes.add(superheroDatabaseProvider.create(EnhancedRandom.random(Superhero.class)));
            }

            List<Superhero> actual = superheroDatabaseProvider.getList(0);
            assertNotNull(actual);
        }

    }

}
