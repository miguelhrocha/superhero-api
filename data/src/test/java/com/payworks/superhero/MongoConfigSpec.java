package com.payworks.superhero;

public interface MongoConfigSpec {

    int port();
    String hostname();

}
