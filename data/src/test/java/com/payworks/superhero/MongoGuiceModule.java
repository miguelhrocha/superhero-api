package com.payworks.superhero;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.payworks.superhero.annotations.MongoSuperheroCollection;
import com.payworks.superhero.annotations.MongoSuperheroDatabase;
import com.payworks.superhero.mappers.AllyMapper;
import com.payworks.superhero.mappers.AllyMapperImpl;
import com.payworks.superhero.mappers.SuperheroMapper;
import com.payworks.superhero.mappers.SuperheroMapperImpl;
import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.ConfigurationSource;
import org.cfg4j.source.classpath.ClasspathConfigurationSource;
import org.cfg4j.source.context.filesprovider.ConfigFilesProvider;

import java.nio.file.Paths;

public class MongoGuiceModule extends AbstractModule {

    @Override
    protected void configure() {

        ConfigurationSource source = new ClasspathConfigurationSource(getConfig());
        ConfigurationProvider provider = new ConfigurationProviderBuilder()
                .withConfigurationSource(source)
                .build();

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        bind(ObjectMapper.class).toInstance(objectMapper);

        MongoConfigSpec configSpec = provider.bind("mongo", MongoConfigSpec.class);
        SuperheroConfigSpec superheroConfigSpec = provider.bind("superhero", SuperheroConfigSpec.class);

        bind(MongoConfigSpec.class).toInstance(configSpec);

        bindConstant().annotatedWith(MongoSuperheroDatabase.class).to(superheroConfigSpec.database());
        bindConstant().annotatedWith(MongoSuperheroCollection.class).to(superheroConfigSpec.collection());

        bind(AllyMapper.class).to(AllyMapperImpl.class);
        bind(SuperheroMapper.class).to(SuperheroMapperImpl.class);
    }

    @Provides
    @Singleton
    public MongoClient provideMongoClient(
            MongoConfigSpec mongoConfigSpec
    ) {
        MongoClientURI mongoClientURI;

        if (System.getenv("MONGODB_URI") != null) {
            mongoClientURI = new MongoClientURI(System.getenv("MONGODB_URI"));
        }
        else {
            String connectionString = String.format(
                    "mongodb://%s:%s/",
                    mongoConfigSpec.hostname(),
                    mongoConfigSpec.port()
            );
            mongoClientURI = new MongoClientURI(connectionString);
        }

        return new MongoClient(
                mongoClientURI
        );
    }

    private ConfigFilesProvider getConfig() {
        String env = System.getenv("app_env");

        String configPathString = "config" +
                (env == null || env.isEmpty() ? "" : String.format(".%s", env)) +
                ".properties";

        return () -> Paths.get(configPathString);
    }

}
