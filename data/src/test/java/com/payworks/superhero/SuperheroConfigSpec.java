package com.payworks.superhero;

public interface SuperheroConfigSpec {

    String database();
    String collection();

}
