FROM openjdk:10-jre-slim

ENV APPLICATION_USER payworks

RUN adduser --disabled-password --gecos '' $APPLICATION_USER

RUN mkdir /app
RUN chown -R $APPLICATION_USER /app

USER $APPLICATION_USER

EXPOSE 8080

COPY server/build/libs/server.jar /app/superhero.jar
WORKDIR /app

CMD ["-server", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-XX:InitialRAMFraction=2", "-XX:MinRAMFraction=2", "-XX:MaxRAMFraction=2", "-XX:+UseG1GC", "-XX:MaxGCPauseMillis=100", "-XX:+UseStringDeduplication", "-jar", "superhero.jar"]
ENTRYPOINT ["java"]