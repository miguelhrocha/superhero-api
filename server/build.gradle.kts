import com.avast.gradle.dockercompose.DockerComposePlugin
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

buildscript {
    repositories {
        jcenter()
    }
}

plugins {
    application
    id("com.github.johnrengelman.shadow")
}

application {
    mainClassName = "com.payworks.superhero.Main"
}

val jacksonVersion = "2.9.6"

dependencies {
    compile(project(":api"))
    compile(project(":impl"))
    compile(project(":data"))

    compile("io.swagger.core.v3", "swagger-jaxrs2", "2.0.0")
    compile("io.swagger.core.v3", "swagger-jaxrs2-servlet-initializer", "2.0.0")

    compile("com.google.inject", "guice", "4.2.0")
    compile("com.google.inject.extensions", "guice-servlet", "4.2.0")

    compile("org.glassfish.jersey.containers", "jersey-container-servlet", "2.27")
    compile("org.glassfish.jersey.inject", "jersey-hk2", "2.27")
    compile("org.glassfish.hk2", "guice-bridge", "2.4.0")
    compile("org.glassfish.jersey.media", "jersey-media-json-jackson", "2.27")

    compile("com.fasterxml.jackson.datatype:jackson-datatype-jsr310:$jacksonVersion")
    compile("com.fasterxml.jackson.core:jackson-core:$jacksonVersion")
    compile("com.fasterxml.jackson.core:jackson-databind:$jacksonVersion")
    compile("com.fasterxml.jackson.jaxrs:jackson-jaxrs-json-provider:$jacksonVersion")

    compile("org.slf4j", "slf4j-api", "1.7.25")
    compile("org.slf4j", "slf4j-log4j12", "1.7.25")

    compile("org.cfg4j", "cfg4j-core", "4.4.1")

    compile("org.eclipse.jetty", "jetty-server", "9.4.11.v20180605")
    compile("org.eclipse.jetty", "jetty-servlet", "9.4.11.v20180605")
    compile("org.eclipse.jetty", "jetty-webapp", "9.4.11.v20180605")

    compile("org.mongodb", "mongo-java-driver", "3.8.1")

    compile("javax.servlet", "javax.servlet-api", "4.0.1")

    compile("com.fasterxml.jackson.jaxrs", "jackson-jaxrs-json-provider", "2.9.6")

    compile("javax.activation", "activation", "1.1.1")
    compile("javax.xml.bind", "jaxb-api", "2.3.0")

    testCompile("org.junit.jupiter", "junit-jupiter-api", "5.2.0")
    testCompile("org.junit.jupiter", "junit-jupiter-engine", "5.2.0")
}

val shadowJar by tasks.getting(ShadowJar::class) {
    classifier = ""
    version = ""
}

val composeUp by rootProject.tasks
composeUp.dependsOn(shadowJar)
