package com.payworks.superhero.mongo;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.payworks.superhero.annotations.MongoSuperheroCollection;
import com.payworks.superhero.annotations.MongoSuperheroDatabase;
import com.payworks.superhero.mappers.AllyMapper;
import com.payworks.superhero.mappers.AllyMapperImpl;
import com.payworks.superhero.mappers.SuperheroMapper;
import com.payworks.superhero.mappers.SuperheroMapperImpl;
import org.cfg4j.provider.ConfigurationProvider;
import org.cfg4j.provider.ConfigurationProviderBuilder;
import org.cfg4j.source.ConfigurationSource;
import org.cfg4j.source.classpath.ClasspathConfigurationSource;
import org.cfg4j.source.context.filesprovider.ConfigFilesProvider;

import java.nio.file.Paths;

public class MongoGuiceModule extends AbstractModule {

    @Override
    protected void configure() {
        ConfigurationSource source = new ClasspathConfigurationSource(getConfig());
        ConfigurationProvider provider = new ConfigurationProviderBuilder()
                .withConfigurationSource(source)
                .build();

        SuperheroConfigSpec superheroConfigSpec = provider.bind("superhero", SuperheroConfigSpec.class);

        bindConstant().annotatedWith(MongoSuperheroDatabase.class).to(superheroConfigSpec.database());
        bindConstant().annotatedWith(MongoSuperheroCollection.class).to(superheroConfigSpec.collection());

        bind(AllyMapper.class).to(AllyMapperImpl.class);
        bind(SuperheroMapper.class).to(SuperheroMapperImpl.class);
    }

    @Provides
    @Singleton
    public MongoClient provideMongoClient() {
        MongoClientURI mongoClientURI = new MongoClientURI(System.getenv("MONGODB_URI"));
        return new MongoClient(
                mongoClientURI
        );
    }

    private ConfigFilesProvider getConfig() {
        String env = System.getenv("app_env");

        String configPathString = "config" +
                (env == null || env.isEmpty() ? "" : String.format(".%s", env)) +
                ".properties";

        return () -> Paths.get(configPathString);
    }

}
