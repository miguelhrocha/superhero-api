package com.payworks.superhero.mongo;

public interface MongoConfigSpec {

    int port();
    String hostname();

}
