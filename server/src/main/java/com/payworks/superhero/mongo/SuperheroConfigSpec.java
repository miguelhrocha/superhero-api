package com.payworks.superhero.mongo;

public interface SuperheroConfigSpec {

    String database();
    String collection();

}
