package com.payworks.superhero;

import com.google.inject.Guice;
import com.google.inject.Module;
import com.payworks.superhero.api.v1.HealthCheckApi;
import com.payworks.superhero.api.v1.SuperheroApi;
import com.payworks.superhero.configuration.JerseyConfiguration;
import com.payworks.superhero.infrastructure.mappers.exceptions.DateTimeParseExceptionMapper;
import com.payworks.superhero.infrastructure.mappers.exceptions.SuperheroNotFoundExceptionMapper;
import com.payworks.superhero.mongo.MongoGuiceModule;
import io.swagger.v3.jaxrs2.integration.resources.AcceptHeaderOpenApiResource;
import io.swagger.v3.jaxrs2.integration.resources.OpenApiResource;

import javax.annotation.concurrent.ThreadSafe;
import java.util.ArrayList;
import java.util.List;

@ThreadSafe
public class Launcher {

    private static final String APPLICATION_PATH = "/api";

    public void run() throws Exception {

        int port = 8080;

        if (System.getenv("PORT") != null)
            port = Integer.parseInt(System.getenv("PORT"));

        JerseyConfiguration configuration = JerseyConfiguration.builder()
                .withContextPath(APPLICATION_PATH)
                .addResourceClass(HealthCheckApi.class)
                .addResourceClass(SuperheroApi.class)
                .addResourceClass(OpenApiResource.class)
                .addResourceClass(AcceptHeaderOpenApiResource.class)
                .addPackage("com.payworks.superhero.infrastructure.mappers.exceptions")
                .addPort(port)
                .build();

        List<Module> modules = new ArrayList<>();
        modules.add(new JerseyModule(configuration));
        modules.add(new SuperheroModule());
        modules.add(new MongoGuiceModule());

        Guice.createInjector(modules).getInstance(JerseyServer.class).start();
    }

}
