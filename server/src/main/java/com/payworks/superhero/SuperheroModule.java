package com.payworks.superhero;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.multibindings.Multibinder;
import com.payworks.superhero.api.v1.HealthCheckApi;
import com.payworks.superhero.api.v1.HealthCheckApiImpl;
import com.payworks.superhero.api.v1.SuperheroApiImpl;
import com.payworks.superhero.api.v1.SuperheroApi;
import com.payworks.superhero.application.RequestHandler;
import com.payworks.superhero.application.superhero.*;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.mappers.exceptions.SuperheroNotFoundExceptionMapper;
import com.payworks.superhero.infrastructure.services.CreateSuperhero;
import com.payworks.superhero.infrastructure.services.GetSuperhero;
import com.payworks.superhero.infrastructure.services.GetSuperheroList;
import com.payworks.superhero.infrastructure.services.ValidateAllies;
import com.payworks.superhero.infrastructure.mappers.entities.AllyMapper;
import com.payworks.superhero.infrastructure.mappers.entities.AllyMapperImpl;
import com.payworks.superhero.infrastructure.mappers.entities.SkillMapper;
import com.payworks.superhero.infrastructure.mappers.entities.SkillMapperImpl;
import io.swagger.v3.jaxrs2.integration.JaxrsOpenApiContextBuilder;
import io.swagger.v3.oas.integration.OpenApiConfigurationException;
import io.swagger.v3.oas.integration.SwaggerConfiguration;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.glassfish.jersey.internal.ExceptionMapperFactory;

import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SuperheroModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(HealthCheckApi.class).to(HealthCheckApiImpl.class);
        bind(SuperheroApi.class).to(SuperheroApiImpl.class);

        bind(new TypeLiteral<RequestHandler<CreateSuperheroCommand, CreateSuperheroCommandResponse>>() {})
                .to(CreateSuperheroCommandHandler.class);

        bind(new TypeLiteral<RequestHandler<GetSuperheroQuery, GetSuperheroQueryResponse>>() {})
                .to(GetSuperheroQueryHandler.class);

        bind(new TypeLiteral<RequestHandler<GetSuperheroListQuery, GetSuperheroListQueryResponse>>() {})
                .to(GetSuperheroListQueryHandler.class);

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);

        JacksonJsonProvider jsonProvider = new JacksonJsonProvider();
        jsonProvider.setMapper(objectMapper);

        bind(ObjectMapper.class).toInstance(objectMapper);
        bind(JacksonJsonProvider.class).toInstance(jsonProvider);

        bind(new TypeLiteral<Supplier<SuperheroId>>() {})
                .toInstance(() -> new SuperheroId(UUID.randomUUID()));

        bind(CreateSuperhero.class).to(SuperheroDatabaseProvider.class);
        bind(ValidateAllies.class).to(SuperheroDatabaseProvider.class);
        bind(GetSuperhero.class).to(SuperheroDatabaseProvider.class);
        bind(GetSuperheroList.class).to(SuperheroDatabaseProvider.class);

        bind(AllyMapper.class).to(AllyMapperImpl.class);
        bind(SkillMapper.class).to(SkillMapperImpl.class);

        OpenAPI oas = new OpenAPI();

        Info info = new Info()
                .title("Superhero API!")
                .version("v1")
                .description("Super cool API")
                .license(new License()
                        .name("Apache 2.0"));

        oas.info(info);

        SwaggerConfiguration oasConfig = new SwaggerConfiguration()
                .openAPI(oas)
                .prettyPrint(true)
                .resourcePackages(Stream.of("com.payworks.superhero").collect(Collectors.toSet()));

        try {
            new JaxrsOpenApiContextBuilder()
                    .openApiConfiguration(oasConfig)
                    .buildContext(true);
        } catch (OpenApiConfigurationException e) {
            e.printStackTrace();
        }
    }

}

