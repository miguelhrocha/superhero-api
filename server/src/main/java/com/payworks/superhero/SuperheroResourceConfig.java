package com.payworks.superhero;

import com.google.inject.Injector;
import com.payworks.superhero.configuration.JerseyConfiguration;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ResourceConfig;
import org.jvnet.hk2.guice.bridge.api.GuiceBridge;
import org.jvnet.hk2.guice.bridge.api.GuiceIntoHK2Bridge;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.ApplicationPath;

@ApplicationPath("/api")
public class SuperheroResourceConfig extends ResourceConfig {

    @Inject
    public SuperheroResourceConfig(ServiceLocator serviceLocator, ServletContext servletContext) {
        super(getResourceConfigFromGuice(servletContext));

        Injector injector = (Injector) servletContext.getAttribute(Injector.class.getName());
        GuiceBridge.getGuiceBridge().initializeGuiceBridge(serviceLocator);
        GuiceIntoHK2Bridge guiceBridge = serviceLocator.getService(GuiceIntoHK2Bridge.class);
        guiceBridge.bridgeGuiceInjector(injector);
    }

    private static ResourceConfig getResourceConfigFromGuice(ServletContext servletContext) {
        Injector injector = (Injector) servletContext.getAttribute(Injector.class.getName());
        JerseyConfiguration jerseyConfiguration = injector.getInstance(JerseyConfiguration.class);
        return jerseyConfiguration.getResourceConfig();
    }

}
