package com.payworks.superhero;

import org.apache.log4j.BasicConfigurator;

public class Main {

    public static void main(String[] args) throws Exception {
        BasicConfigurator.configure();
        Launcher launcher = new Launcher();
        launcher.run();
    }

}
