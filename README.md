# Superhero API

RESTful API to create and know about all your favorite superheroes! The application follows some of the design principles of the Clean Architecture by Robert C. Martin, it's structured with Domain-Driven Design and implements the CQRS pattern.

## Frameworks in motion

The application runs on Java 10.

### Jersey + Swagger + Guice + MongoDB + Jetty

Taking in consideration the small requirements I wanted to go for a lightway framework. My first option was [Javalin](http://javalin.io) but I decided that going with a JAX-RS standard would because that would open the possibilities of switching between any kind of JAX-RS compliant framework (CXF, Jersey, RESTeasy).

Another benefit of using a JAX-RS framework was the implementation of Swagger as our tool to automate the documentation process. Swagger is a must by today standards, the OpenAPI Spec would remove all the hassle in distributing the API with tools like the [Swagger Codegen](https://github.com/swagger-api/swagger-codegen)

The Inversion of Control principle is an efficient and helpful pattern where we can achieve a high level of decoupling between components. Google Guice was more a matter of personal preference than anything else, in the modern development days every DI frameworks it's pretty similar.

I chose a NoSQL database because the requirements shaped the model like that, there wasn't necessary any kind of relationship between the entities or any kind of transaction process. I just needed a data store where to put my superheroes and I think any NoSQL engine should have done the job.

Then, why MongoDB and not other provider like DynamoDB? At this point in time we just need a simple way of storing data, DynamoDB it's too complex for our needs and there are several design decisions in the overall API that made me discard DynamoDB quickly enough. MongoDB checks all the marks in our use case:

* No joins
* Fast writes
* Easy queries to solve our use cases

The architecture implemented gives the possibility of use any database provider without messing with the model nor the business rules.

As for Jetty, I wanted something lightweight so Tomcat was discarded right away, the only option I saw was Netty but that's more oriented towards event-driven applications.

## I want to run it in my computer

```bash
./gradlew composeUp
```

And that's it! The first time you run it it will take a little, but after that everything should be up and running in seconds.

## I want to use it

The first time you access a resource in the application it'll take some time to spin up the container (free tier account), after that it will run smoother. I recommend hitting the [healthcheck endpoint](https://payworks-superhero-api.herokuapp.com/api/public/v1/healthcheck) before start using it.

Go to the docs to see how to consume the API:
https://payworks-superhero-api.herokuapp.com/api/openapi.json

## Time spent

* The base infrastructure was done in a day
* The implementation of all the endpoints was done in one and a half days
* Refactor and overall clean up process was done in 3/4 of day

## What I could have done to finish the optional tasks

### Authentication

Unfortunately I didn't got the chance to implement the authentication feature, but I would have implemented the following filter:

https://github.com/swagger-api/swagger-samples/blob/2.0/java/java-jersey2-configfile/src/main/java/io/swagger/sample/util/ApiAuthorizationFilterImpl.java

This meant also implementing the User endpoint and CQRS operations.

### HTTPS

HTTPS works thanks to Heroku, I didn't got the chance to research about how to make the SSL termination on Jetty.