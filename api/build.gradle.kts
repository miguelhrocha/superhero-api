plugins {
    java
}

dependencies {
    compile("io.swagger.core.v3", "swagger-jaxrs2", "2.0.0")
    compile("javax.ws.rs", "javax.ws.rs-api", "2.1")
}
