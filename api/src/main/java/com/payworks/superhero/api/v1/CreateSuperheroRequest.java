package com.payworks.superhero.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class CreateSuperheroRequest {

    private final String name;
    private final String pseudonym;
    private final String publisher;
    private final List<String> skills;
    private final List<AllyTransferObject> allies;
    private final String firstAppearanceDate;

    @JsonCreator
    public CreateSuperheroRequest(
            @NotNull @JsonProperty("name") String name,
            @NotNull @JsonProperty("pseudonym") String pseudonym,
            @NotNull @JsonProperty("publisher") String publisher,
            @NotNull @JsonProperty("skills") List<String> skills,
            @JsonProperty("allies") List<AllyTransferObject> allies,
            @NotNull @JsonProperty("firstAppearanceDate") String firstAppearanceDate
    ) {
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
        this.skills = skills;
        this.allies = allies;
        this.firstAppearanceDate = firstAppearanceDate;
    }

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public String getPublisher() {
        return publisher;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<AllyTransferObject> getAllies() {
        return allies;
    }

    public String getFirstAppearanceDate() {
        return firstAppearanceDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateSuperheroRequest that = (CreateSuperheroRequest) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(pseudonym, that.pseudonym) &&
                Objects.equals(publisher, that.publisher) &&
                Objects.equals(skills, that.skills) &&
                Objects.equals(allies, that.allies) &&
                Objects.equals(firstAppearanceDate, that.firstAppearanceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, pseudonym, publisher, skills, allies, firstAppearanceDate);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CreateSuperheroRequest.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("pseudonym='" + pseudonym + "'")
                .add("publisher='" + publisher + "'")
                .add("skills=" + skills)
                .add("allies=" + allies)
                .add("firstAppearanceDate='" + firstAppearanceDate + "'")
                .toString();
    }
}
