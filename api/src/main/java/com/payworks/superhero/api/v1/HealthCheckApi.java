package com.payworks.superhero.api.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/public/v1")
public interface HealthCheckApi {

    @GET
    @Path("healthcheck")
    @Produces(MediaType.TEXT_PLAIN)
    @Operation(
            summary = "Checks health of Superhero API",
            tags = {"healthcheck"},
            responses = {
                    @ApiResponse(responseCode = "200", description = "Everything's healthy")
            }
    )
    String checkHealth();

}
