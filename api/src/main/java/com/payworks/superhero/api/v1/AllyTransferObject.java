package com.payworks.superhero.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;

public class AllyTransferObject {

    private final UUID id;
    private final String name;

    @JsonCreator
    public AllyTransferObject(
            @JsonProperty("id") UUID id,
            @JsonProperty("name") String name
    ) {
        this.id = id;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AllyTransferObject allyTransferObject = (AllyTransferObject) o;
        return Objects.equals(id, allyTransferObject.id) &&
                Objects.equals(name, allyTransferObject.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", AllyTransferObject.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .toString();
    }
}
