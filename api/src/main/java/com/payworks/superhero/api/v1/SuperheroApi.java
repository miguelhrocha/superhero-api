package com.payworks.superhero.api.v1;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/public/v1/superhero")
public interface SuperheroApi {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes({ MediaType.APPLICATION_JSON })
    @Operation(
            summary = "Create a superhero",
            operationId = "createSuperhero",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "The superhero created",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON,
                                    schema = @Schema(implementation = CreateSuperheroResponse.class)
                            )
                    ),
                    @ApiResponse(
                            responseCode = "400",
                            description = "Ally does not exist or there's an invalid parameter (date format, publisher, missing value, etc)."
                    )
            }
    ) CreateSuperheroResponse createSuperhero(
            @RequestBody(
                    description = "The superhero to be created",
                    required = true,
                    content = @Content(schema = @Schema(implementation = CreateSuperheroRequest.class))
            )
            @NotNull CreateSuperheroRequest createSuperheroRequest
    );

    @GET
    @Path("/{superheroId}")
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Get an specific superhero with the id provided in the create superhero endpoint",
            operationId = "createSuperhero",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "The superhero retrieved",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON,
                                    schema = @Schema(implementation = GetSuperheroResponse.class)
                            )
                    ),
                    @ApiResponse(responseCode = "400", description = "superheroId is not a valid UUID"),
                    @ApiResponse(responseCode = "404", description = "superhero does not exist")
            }
    )
    GetSuperheroResponse getSuperhero(
            @Parameter(
                    description = "The superhero id provided during creation, use c0bbda5b-9be8-43aa-995e-4613c5bd31d2 for testing",
                    required = true,
                    schema = @Schema(type = "string", format = "uuid")
            )
            @NotNull @PathParam("superheroId") String superheroId
    );


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Operation(
            summary = "Get a list of superhero",
            description = "Gets a page of superheroes, the page size is 10",
            responses = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "The list of superheroes",
                            content = @Content(
                                    mediaType = MediaType.APPLICATION_JSON,
                                    schema = @Schema(implementation = GetSuperheroListResponse.class)
                            )
                    ),
                    @ApiResponse(responseCode = "400", description = "offset must be a positive number")
            }
    )
    GetSuperheroListResponse getSuperheroList(
            @Parameter(
                    description = "The page you want to retrieve, by default it will bring the first page if you don't send this param.",
                    schema = @Schema(type = "int")
            )
            @DefaultValue("0") @QueryParam("offset") int offset
    );

}