package com.payworks.superhero.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class GetSuperheroListResponse {

    private final List<GetSuperheroResponse> superheroes;

    @JsonCreator
    public GetSuperheroListResponse(
            @JsonProperty("superheroes") List<GetSuperheroResponse> superheroes
    ) {
        this.superheroes = superheroes;
    }

    public List<GetSuperheroResponse> getSuperheroes() {
        return superheroes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSuperheroListResponse that = (GetSuperheroListResponse) o;
        return Objects.equals(superheroes, that.superheroes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(superheroes);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetSuperheroListResponse.class.getSimpleName() + "[", "]")
                .add("superheroes=" + superheroes)
                .toString();
    }

}
