package com.payworks.superhero.api.v1;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class GetSuperheroResponse {

    private final String id;
    private final String name;
    private final String pseudonym;
    private final String publisher;
    private final List<String> skills;
    private final List<AllyTransferObject> allies;
    private final String appearanceDate;

    @JsonCreator
    public GetSuperheroResponse(
            @JsonProperty("id") String id,
            @JsonProperty("name") String name,
            @JsonProperty("pseudonym") String pseudonym,
            @JsonProperty("publisher") String publisher,
            @JsonProperty("skills") List<String> skills,
            @JsonProperty("allies") List<AllyTransferObject> allies,
            @JsonProperty("appearanceDate") String appearanceDate
    ) {
        this.id = id;
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
        this.skills = skills;
        this.allies = allies;
        this.appearanceDate = appearanceDate;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public String getPublisher() {
        return publisher;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<AllyTransferObject> getAllies() {
        return allies;
    }

    public String getAppearanceDate() {
        return appearanceDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSuperheroResponse that = (GetSuperheroResponse) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(pseudonym, that.pseudonym) &&
                Objects.equals(publisher, that.publisher) &&
                Objects.equals(skills, that.skills) &&
                Objects.equals(allies, that.allies) &&
                Objects.equals(appearanceDate, that.appearanceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, pseudonym, publisher, skills, allies, appearanceDate);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetSuperheroResponse.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .add("pseudonym='" + pseudonym + "'")
                .add("publisher='" + publisher + "'")
                .add("skills=" + skills)
                .add("allies=" + allies)
                .add("appearanceDate='" + appearanceDate + "'")
                .toString();
    }
}
