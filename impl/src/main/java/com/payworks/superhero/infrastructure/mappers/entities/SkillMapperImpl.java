package com.payworks.superhero.infrastructure.mappers.entities;

import com.payworks.superhero.domain.model.Skill;

import java.util.List;
import java.util.stream.Collectors;

public class SkillMapperImpl implements SkillMapper {

    @Override
    public String mapToTransferObject(Skill skill) {
        return skill.getDescription();
    }

    @Override
    public Skill mapToModel(String transferObject) {
        return new Skill(transferObject);
    }

    @Override
    public List<String> mapToCollectionOfTransferObjects(List<Skill> skills) {
        return skills.stream().map(this::mapToTransferObject).collect(Collectors.toList());
    }

    @Override
    public List<Skill> mapToCollectionOfModels(List<String> transferObjects) {
        return transferObjects.stream().map(this::mapToModel).collect(Collectors.toList());
    }
}
