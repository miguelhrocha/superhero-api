package com.payworks.superhero.infrastructure.mappers.entities;

import com.payworks.superhero.domain.model.Skill;

public interface SkillMapper extends Mapper<String, Skill> {

}
