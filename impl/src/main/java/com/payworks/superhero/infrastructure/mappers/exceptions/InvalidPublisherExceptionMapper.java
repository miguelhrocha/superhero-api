package com.payworks.superhero.infrastructure.mappers.exceptions;

import com.payworks.superhero.application.superhero.InvalidPublisherException;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class InvalidPublisherExceptionMapper implements ExceptionMapper<InvalidPublisherException> {

    @Override
    public Response toResponse(InvalidPublisherException exception) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity("Publisher must be either DC or MARVEL (case sensitive)")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

}
