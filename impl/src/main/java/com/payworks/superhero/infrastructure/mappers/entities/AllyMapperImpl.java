package com.payworks.superhero.infrastructure.mappers.entities;

import com.payworks.superhero.api.v1.AllyTransferObject;
import com.payworks.superhero.domain.model.Ally;
import com.payworks.superhero.domain.model.SuperheroId;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AllyMapperImpl implements AllyMapper {

    @Override
    public AllyTransferObject mapToTransferObject(Ally ally) {
        return new AllyTransferObject(ally.getSuperheroId().getId(), ally.getName());
    }

    @Override
    public Ally mapToModel(AllyTransferObject transferObject) {
        return new Ally(new SuperheroId(transferObject.getId()), transferObject.getName());
    }

    @Override
    public List<AllyTransferObject> mapToCollectionOfTransferObjects(List<Ally> allies) {
        return allies.stream()
                .map(this::mapToTransferObject)
                .collect(Collectors.toList());
    }

    @Override
    public List<Ally> mapToCollectionOfModels(List<AllyTransferObject> transferObjects) {
        if (transferObjects == null)
            return new ArrayList<>();
        else
            return transferObjects.stream()
                .map(this::mapToModel)
                .collect(Collectors.toList());
    }

}
