package com.payworks.superhero.infrastructure.mappers.exceptions;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.time.format.DateTimeParseException;

@Provider
@Singleton
public class DateTimeParseExceptionMapper implements ExceptionMapper<DateTimeParseException> {

    @Override
    public Response toResponse(DateTimeParseException exception) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity("Entity is not compliant with YYYY-MM-DD format")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

}
