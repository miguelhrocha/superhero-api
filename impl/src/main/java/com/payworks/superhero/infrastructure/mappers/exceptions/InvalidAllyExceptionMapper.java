package com.payworks.superhero.infrastructure.mappers.exceptions;

import com.payworks.superhero.application.superhero.InvalidAllyException;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class InvalidAllyExceptionMapper implements ExceptionMapper<InvalidAllyException> {

    @Override
    public Response toResponse(InvalidAllyException exception) {
        return Response
                .status(Response.Status.BAD_REQUEST)
                .entity("Some ally in the list does not exist in the application")
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

}
