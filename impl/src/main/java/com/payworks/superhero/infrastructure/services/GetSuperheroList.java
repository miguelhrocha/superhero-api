package com.payworks.superhero.infrastructure.services;

import com.payworks.superhero.domain.model.Superhero;

import java.util.List;

public interface GetSuperheroList {

    List<Superhero> getList(int offset);

}
