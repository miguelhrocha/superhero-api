package com.payworks.superhero.infrastructure.services;

import com.payworks.superhero.domain.model.Ally;

import java.util.List;

public interface ValidateAllies {

    boolean validate(List<Ally> allies);

}
