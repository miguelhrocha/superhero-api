package com.payworks.superhero.infrastructure.mappers.entities;

import com.payworks.superhero.api.v1.AllyTransferObject;
import com.payworks.superhero.domain.model.Ally;

public interface AllyMapper extends Mapper<AllyTransferObject, Ally> {
}
