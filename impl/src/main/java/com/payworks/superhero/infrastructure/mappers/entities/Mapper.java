package com.payworks.superhero.infrastructure.mappers.entities;

import java.util.List;

public interface Mapper<TTransferObject, TModel> {

    TTransferObject mapToTransferObject(TModel model);

    TModel mapToModel(TTransferObject transferObject);

    List<TTransferObject> mapToCollectionOfTransferObjects(List<TModel> models);

    List<TModel> mapToCollectionOfModels(List<TTransferObject> transferObjects);

}
