package com.payworks.superhero.infrastructure.services;

import com.payworks.superhero.domain.model.Superhero;

public interface CreateSuperhero {

    Superhero create(Superhero model);

}
