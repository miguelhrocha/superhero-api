package com.payworks.superhero.infrastructure.services;

import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;

public interface GetSuperhero {

    Superhero get(SuperheroId superheroId);

}
