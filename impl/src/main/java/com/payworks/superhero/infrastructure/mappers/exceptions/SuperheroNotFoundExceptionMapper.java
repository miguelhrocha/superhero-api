package com.payworks.superhero.infrastructure.mappers.exceptions;

import com.payworks.superhero.application.superhero.SuperheroNotFoundException;

import javax.inject.Singleton;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Singleton
public class SuperheroNotFoundExceptionMapper implements ExceptionMapper<SuperheroNotFoundException> {

    @Override
    public Response toResponse(SuperheroNotFoundException exception) {
        return Response
                .status(Response.Status.NOT_FOUND)
                .entity(exception.getMessage())
                .type(MediaType.APPLICATION_JSON_TYPE)
                .build();
    }

}
