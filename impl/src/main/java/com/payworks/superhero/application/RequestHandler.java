package com.payworks.superhero.application;

public interface RequestHandler<TRequest extends Request, TResponse> {

    TResponse handle(TRequest request);

}
