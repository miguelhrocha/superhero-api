package com.payworks.superhero.application.superhero;

public class SuperheroNotFoundException extends RuntimeException {

    public SuperheroNotFoundException(String message) {
        super(message);
    }

}
