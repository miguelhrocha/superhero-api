package com.payworks.superhero.application.superhero;

import com.payworks.superhero.application.Request;

import java.util.Objects;
import java.util.StringJoiner;

public class GetSuperheroListQuery implements Request {

    private final int offset;

    public GetSuperheroListQuery(int offset) {
        this.offset = offset;
    }

    public int getOffset() {
        return offset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSuperheroListQuery that = (GetSuperheroListQuery) o;
        return offset == that.offset;
    }

    @Override
    public int hashCode() {
        return Objects.hash(offset);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetSuperheroListQuery.class.getSimpleName() + "[", "]")
                .add("offset=" + offset)
                .toString();
    }

}
