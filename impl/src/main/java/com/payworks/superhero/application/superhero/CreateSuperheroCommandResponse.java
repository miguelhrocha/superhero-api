package com.payworks.superhero.application.superhero;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;

public class CreateSuperheroCommandResponse {

    private final String id;
    private final String name;

    public CreateSuperheroCommandResponse(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateSuperheroCommandResponse that = (CreateSuperheroCommandResponse) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CreateSuperheroCommandResponse.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("name='" + name + "'")
                .toString();
    }
}
