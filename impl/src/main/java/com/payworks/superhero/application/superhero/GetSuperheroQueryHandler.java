package com.payworks.superhero.application.superhero;

import com.payworks.superhero.application.RequestHandler;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.mappers.entities.AllyMapper;
import com.payworks.superhero.infrastructure.mappers.entities.SkillMapper;
import com.payworks.superhero.infrastructure.services.GetSuperhero;

import javax.inject.Inject;

public class GetSuperheroQueryHandler implements RequestHandler<GetSuperheroQuery, GetSuperheroQueryResponse> {

    private final GetSuperhero getSuperhero;
    private final SkillMapper skillMapper;
    private final AllyMapper allyMapper;

    @Inject
    public GetSuperheroQueryHandler(GetSuperhero getSuperhero, SkillMapper skillMapper, AllyMapper allyMapper) {
        this.getSuperhero = getSuperhero;
        this.skillMapper = skillMapper;
        this.allyMapper = allyMapper;
    }

    @Override
    public GetSuperheroQueryResponse handle(GetSuperheroQuery request) {
        SuperheroId superheroId = request.getSuperheroId();

        Superhero superhero = getSuperhero.get(superheroId);

        if (superhero == null)
            throw new SuperheroNotFoundException(String.format("Superhero with id: %s not found", superheroId.getIdString()));

        return new GetSuperheroQueryResponse(
                superhero.getSuperheroId().getIdString(),
                superhero.getName(),
                superhero.getPseudonym(),
                superhero.getPublisher().toString(),
                skillMapper.mapToCollectionOfTransferObjects(superhero.getSkills()),
                allyMapper.mapToCollectionOfTransferObjects(superhero.getAllies()),
                superhero.getAppearanceDate().toString()
        );
    }

}
