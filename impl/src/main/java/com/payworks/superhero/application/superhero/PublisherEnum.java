package com.payworks.superhero.application.superhero;

import java.util.Arrays;

public enum PublisherEnum {
    MARVEL,
    DC;

    public static boolean isValid(String value) {
        return Arrays.stream(PublisherEnum.values()).anyMatch(p -> p.toString().equals(value));
    }
}
