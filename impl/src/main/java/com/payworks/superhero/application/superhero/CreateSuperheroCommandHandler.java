package com.payworks.superhero.application.superhero;

import com.payworks.superhero.application.RequestHandler;
import com.payworks.superhero.domain.model.Ally;
import com.payworks.superhero.domain.model.Skill;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.services.CreateSuperhero;
import com.payworks.superhero.infrastructure.services.ValidateAllies;
import com.payworks.superhero.infrastructure.mappers.entities.AllyMapper;
import com.payworks.superhero.infrastructure.mappers.entities.SkillMapper;

import javax.inject.Inject;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.Validate.notBlank;

public class CreateSuperheroCommandHandler implements
        RequestHandler<CreateSuperheroCommand, CreateSuperheroCommandResponse> {

    private final CreateSuperhero createSuperhero;
    private final ValidateAllies validateAllies;
    private final Supplier<SuperheroId> superheroIdSupplier;

    private final SkillMapper skillMapper;
    private final AllyMapper allyMapper;

    @Inject
    public CreateSuperheroCommandHandler(
            CreateSuperhero createSuperhero,
            ValidateAllies validateAllies,
            Supplier<SuperheroId> superheroIdSupplier,
            SkillMapper skillMapper,
            AllyMapper allyMapper
    ) {
        this.createSuperhero = createSuperhero;
        this.validateAllies = validateAllies;
        this.superheroIdSupplier = superheroIdSupplier;
        this.skillMapper = skillMapper;
        this.allyMapper = allyMapper;
    }

    @Override
    public CreateSuperheroCommandResponse handle(CreateSuperheroCommand request) {
        validateCommand(request);

        List<Skill> skills = skillMapper.mapToCollectionOfModels(request.getSkills());
        List<Ally> allies = allyMapper.mapToCollectionOfModels(request.getAllies());

        Superhero model = new Superhero(
                superheroIdSupplier.get(),
                request.getName(),
                request.getPseudonym(),
                PublisherEnum.valueOf(request.getPublisher()),
                skills,
                allies,
                LocalDate.parse(request.getFirstAppearanceDate())
        );

        Superhero response = createSuperhero.create(model);

        return new CreateSuperheroCommandResponse(
                response.getSuperheroId().getIdString(),
                response.getName()
        );
    }

    private void validateCommand(CreateSuperheroCommand command) {
        notBlank(command.getName(), "Superhero name cannot be null");
        notBlank(command.getFirstAppearanceDate(), "Appearance date cannot be null or blank");

        if (!PublisherEnum.isValid(command.getPublisher())) {
            throw new InvalidPublisherException();
        }

        LocalDate.parse(command.getFirstAppearanceDate());

        if (command.getAllies() != null) {
            List<Ally> allies = command.getAllies()
                    .stream()
                    .map(a -> new Ally(new SuperheroId(a.getId()), a.getName()))
                    .collect(Collectors.toList());

            boolean alliesAreValid = validateAllies.validate(allies);

            if (!alliesAreValid)
                throw new InvalidAllyException();
        }
    }

}
