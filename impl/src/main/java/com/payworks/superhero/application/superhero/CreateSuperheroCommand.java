package com.payworks.superhero.application.superhero;

import com.payworks.superhero.api.v1.AllyTransferObject;
import com.payworks.superhero.application.Request;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class CreateSuperheroCommand implements Request {

    private final String name;
    private final String pseudonym;
    private final String publisher;
    private final List<String> skills;
    private final List<AllyTransferObject> allies;
    private final String firstAppearanceDate;

    public CreateSuperheroCommand(
            String name,
            String pseudonym,
            String publisher,
            List<String> skills,
            List<AllyTransferObject> allies,
            String firstAppearanceDate
    ) {
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
        this.skills = skills;
        this.allies = allies;
        this.firstAppearanceDate = firstAppearanceDate;
    }

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public String getPublisher() {
        return publisher;
    }

    public List<String> getSkills() {
        return skills;
    }

    public List<AllyTransferObject> getAllies() {
        return allies;
    }

    public String getFirstAppearanceDate() {
        return firstAppearanceDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreateSuperheroCommand that = (CreateSuperheroCommand) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(pseudonym, that.pseudonym) &&
                publisher == that.publisher &&
                Objects.equals(skills, that.skills) &&
                Objects.equals(allies, that.allies) &&
                Objects.equals(firstAppearanceDate, that.firstAppearanceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, pseudonym, publisher, skills, allies, firstAppearanceDate);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", CreateSuperheroCommand.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("pseudonym='" + pseudonym + "'")
                .add("publisher=" + publisher)
                .add("skills=" + skills)
                .add("allies=" + allies)
                .add("firstAppearanceDate=" + firstAppearanceDate)
                .toString();
    }
}
