package com.payworks.superhero.application.superhero;

import com.payworks.superhero.domain.model.Superhero;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class GetSuperheroListQueryResponse {

    private final List<Superhero> superheroes;

    public GetSuperheroListQueryResponse(List<Superhero> superheroes) {
        this.superheroes = superheroes;
    }

    public List<Superhero> getSuperheroes() {
        return superheroes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSuperheroListQueryResponse that = (GetSuperheroListQueryResponse) o;
        return Objects.equals(superheroes, that.superheroes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(superheroes);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetSuperheroListQueryResponse.class.getSimpleName() + "[", "]")
                .add("superheroes=" + superheroes)
                .toString();
    }

}
