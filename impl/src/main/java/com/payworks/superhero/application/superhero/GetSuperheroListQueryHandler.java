package com.payworks.superhero.application.superhero;

import com.payworks.superhero.application.RequestHandler;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.infrastructure.services.GetSuperheroList;

import javax.inject.Inject;
import java.util.List;

public class GetSuperheroListQueryHandler implements RequestHandler<GetSuperheroListQuery, GetSuperheroListQueryResponse> {

    private final GetSuperheroList getSuperheroList;

    @Inject
    public GetSuperheroListQueryHandler(GetSuperheroList getSuperheroList) {
        this.getSuperheroList = getSuperheroList;
    }

    @Override
    public GetSuperheroListQueryResponse handle(GetSuperheroListQuery request) {
        int offset = request.getOffset();

        List<Superhero> superheroes = getSuperheroList.getList(offset);

        return new GetSuperheroListQueryResponse(superheroes);
    }

}
