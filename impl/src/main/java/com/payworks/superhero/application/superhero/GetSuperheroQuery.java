package com.payworks.superhero.application.superhero;

import com.payworks.superhero.application.Request;
import com.payworks.superhero.domain.model.SuperheroId;

import java.util.Objects;
import java.util.StringJoiner;

public class GetSuperheroQuery implements Request {

    private final SuperheroId superheroId;

    public GetSuperheroQuery(SuperheroId superheroId) {
        this.superheroId = superheroId;
    }

    public SuperheroId getSuperheroId() {
        return superheroId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GetSuperheroQuery that = (GetSuperheroQuery) o;
        return Objects.equals(superheroId, that.superheroId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(superheroId);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", GetSuperheroQuery.class.getSimpleName() + "[", "]")
                .add("superheroId=" + superheroId)
                .toString();
    }
}
