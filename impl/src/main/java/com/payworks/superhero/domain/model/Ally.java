package com.payworks.superhero.domain.model;

import java.util.Objects;
import java.util.StringJoiner;

public class Ally {

    private final SuperheroId superheroId;
    private final String name;

    public Ally(SuperheroId superheroId, String name) {
        this.superheroId = superheroId;
        this.name = name;
    }

    public SuperheroId getSuperheroId() {
        return superheroId;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ally ally = (Ally) o;
        return Objects.equals(superheroId, ally.superheroId) &&
                Objects.equals(name, ally.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(superheroId, name);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Ally.class.getSimpleName() + "[", "]")
                .add("superheroId=" + superheroId)
                .add("name='" + name + "'")
                .toString();
    }
}
