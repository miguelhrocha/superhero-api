package com.payworks.superhero.domain.model;

import com.payworks.superhero.application.superhero.PublisherEnum;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public class Superhero {

    private final SuperheroId superheroId;
    private final String name;
    private final String pseudonym;
    private final PublisherEnum publisher;
    private final List<Skill> skills;
    private final List<Ally> allies;
    private final LocalDate appearanceDate;

    public Superhero(
            SuperheroId superheroId,
            String name,
            String pseudonym,
            PublisherEnum publisher,
            List<Skill> skills,
            List<Ally> allies,
            LocalDate appearanceDate
    ) {
        this.superheroId = superheroId;
        this.name = name;
        this.pseudonym = pseudonym;
        this.publisher = publisher;
        this.skills = skills;
        this.appearanceDate = appearanceDate;

        this.allies = Objects.requireNonNullElseGet(allies, ArrayList::new);
    }

    public SuperheroId getSuperheroId() {
        return superheroId;
    }

    public String getName() {
        return name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public PublisherEnum getPublisher() {
        return publisher;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public List<Ally> getAllies() {
        return allies;
    }

    public LocalDate getAppearanceDate() {
        return appearanceDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Superhero superhero = (Superhero) o;
        return Objects.equals(superheroId, superhero.superheroId) &&
                Objects.equals(name, superhero.name) &&
                Objects.equals(pseudonym, superhero.pseudonym) &&
                publisher == superhero.publisher &&
                Objects.equals(skills, superhero.skills) &&
                Objects.equals(allies, superhero.allies) &&
                Objects.equals(appearanceDate, superhero.appearanceDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(superheroId, name, pseudonym, publisher, skills, allies, appearanceDate);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Superhero.class.getSimpleName() + "[", "]")
                .add("superheroId=" + superheroId)
                .add("name='" + name + "'")
                .add("pseudonym='" + pseudonym + "'")
                .add("publisher=" + publisher)
                .add("skills=" + skills)
                .add("allies=" + allies)
                .add("appearanceDate=" + appearanceDate)
                .toString();
    }
}
