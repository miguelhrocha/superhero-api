package com.payworks.superhero.domain.model;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;

public class SuperheroId {

    private final UUID id;

    public SuperheroId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getIdString() {
        return id.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SuperheroId that = (SuperheroId) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", SuperheroId.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .toString();
    }
}
