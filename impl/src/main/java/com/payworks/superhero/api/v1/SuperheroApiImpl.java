package com.payworks.superhero.api.v1;

import com.payworks.superhero.application.RequestHandler;
import com.payworks.superhero.application.superhero.*;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.mappers.entities.AllyMapper;
import com.payworks.superhero.infrastructure.mappers.entities.SkillMapper;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.Validate.notBlank;

public class SuperheroApiImpl implements SuperheroApi {

    private final RequestHandler<CreateSuperheroCommand, CreateSuperheroCommandResponse> createSuperheroCommandHandler;
    private final RequestHandler<GetSuperheroQuery, GetSuperheroQueryResponse> getSuperheroQueryHandler;
    private final RequestHandler<GetSuperheroListQuery, GetSuperheroListQueryResponse> getSuperheroListQueryHandler;

    private final SkillMapper skillMapper;
    private final AllyMapper allyMapper;

    @Inject
    public SuperheroApiImpl(
            RequestHandler<CreateSuperheroCommand, CreateSuperheroCommandResponse> createSuperheroCommandHandler,
            RequestHandler<GetSuperheroQuery, GetSuperheroQueryResponse> geSuperheroQueryHandler,
            RequestHandler<GetSuperheroListQuery, GetSuperheroListQueryResponse> getSuperheroListQueryHandler,
            SkillMapper skillMapper,
            AllyMapper allyMapper
    ) {
        this.createSuperheroCommandHandler = createSuperheroCommandHandler;
        this.getSuperheroQueryHandler = geSuperheroQueryHandler;
        this.getSuperheroListQueryHandler = getSuperheroListQueryHandler;
        this.skillMapper = skillMapper;
        this.allyMapper = allyMapper;
    }

    @Override
    public CreateSuperheroResponse createSuperhero(@NotNull CreateSuperheroRequest request) {
        CreateSuperheroCommand command = new CreateSuperheroCommand(
                request.getName(),
                request.getPseudonym(),
                request.getPublisher(),
                request.getSkills(),
                request.getAllies(),
                request.getFirstAppearanceDate()
        );

        CreateSuperheroCommandResponse response = createSuperheroCommandHandler.handle(command);

        return new CreateSuperheroResponse(
                response.getId(),
                response.getName()
        );

    }

    @Override
    public GetSuperheroResponse getSuperhero(@NotNull String id) {
        notBlank(id);

        SuperheroId superheroId = new SuperheroId(UUID.fromString(id));
        GetSuperheroQuery query = new GetSuperheroQuery(superheroId);

        GetSuperheroQueryResponse response = getSuperheroQueryHandler.handle(query);

        return new GetSuperheroResponse(
                response.getId(),
                response.getName(),
                response.getPseudonym(),
                response.getPublisher(),
                response.getSkills(),
                response.getAllies(),
                response.getAppearanceDate()
        );
    }

    @Override
    public GetSuperheroListResponse getSuperheroList(int offset) {
        if (offset < 0)
            throw new IllegalArgumentException("Offset cannot be less than zero");

        GetSuperheroListQueryResponse queryResponse = getSuperheroListQueryHandler.handle(new GetSuperheroListQuery(offset));

        List<GetSuperheroResponse> getSuperheroQueryResponseList = queryResponse.getSuperheroes()
                .stream()
                .map(s -> new GetSuperheroResponse(
                        s.getSuperheroId().getIdString(),
                        s.getName(),
                        s.getPseudonym(),
                        s.getPublisher().toString(),
                        skillMapper.mapToCollectionOfTransferObjects(s.getSkills()),
                        allyMapper.mapToCollectionOfTransferObjects(s.getAllies()),
                        s.getAppearanceDate().toString()
                ))
                .collect(Collectors.toList());

        return new GetSuperheroListResponse(getSuperheroQueryResponseList);
    }

}
