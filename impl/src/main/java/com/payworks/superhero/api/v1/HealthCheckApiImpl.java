package com.payworks.superhero.api.v1;

public class HealthCheckApiImpl implements HealthCheckApi {
    @Override
    public String checkHealth() {
        return "OK";
    }
}
