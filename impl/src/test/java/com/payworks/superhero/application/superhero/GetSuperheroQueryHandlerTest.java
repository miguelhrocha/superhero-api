package com.payworks.superhero.application.superhero;

import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.mappers.entities.AllyMapper;
import com.payworks.superhero.infrastructure.mappers.entities.SkillMapper;
import com.payworks.superhero.infrastructure.services.GetSuperhero;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import javax.ws.rs.WebApplicationException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class GetSuperheroQueryHandlerTest {

    @Nested
    class handle {

        @InjectMocks
        private GetSuperheroQueryHandler getSuperheroQueryHandler;

        @Mock
        private GetSuperhero getSuperhero;

        @Spy
        private SkillMapper skillMapper;

        @Spy
        private AllyMapper allyMapper;

        private SuperheroId superheroId = EnhancedRandom.random(SuperheroId.class);

        private Superhero superhero = EnhancedRandom.random(Superhero.class);

        @BeforeEach
        public void setUp() {
            when(getSuperhero.get(superheroId)).thenReturn(superhero);
        }

        @Test
        @DisplayName("Verifies that the query handler calls the GetSuperhero interface")
        public void callsGetSuperhero() {
            var getSuperheroQuery = new GetSuperheroQuery(superheroId);

            getSuperheroQueryHandler.handle(getSuperheroQuery);

            verify(getSuperhero).get(superheroId);
        }

        @Test
        @DisplayName("Throws SuperheroNotFoundException when getSuperhero returns null")
        public void throwsSuperheroNotFoundExceptionWhenThereIsNoSuperhero() {
            String expectedMessage = String.format("Superhero with id: %s not found", superheroId.getIdString());

            when(getSuperhero.get(superheroId)).thenReturn(null);

            Throwable throwable = assertThrows(
                    SuperheroNotFoundException.class,
                    () -> getSuperheroQueryHandler.handle(new GetSuperheroQuery(superheroId))
            );

            assertEquals(expectedMessage, throwable.getMessage());
        }

        @Test
        @DisplayName("Returns correct values from get superhero method")
        public void returnsCorrectRetrievedSuperheroValues() {
            GetSuperheroQueryResponse expected= new GetSuperheroQueryResponse(
                    superhero.getSuperheroId().getIdString(),
                    superhero.getName(),
                    superhero.getPseudonym(),
                    superhero.getPublisher().toString(),
                    skillMapper.mapToCollectionOfTransferObjects(superhero.getSkills()),
                    allyMapper.mapToCollectionOfTransferObjects(superhero.getAllies()),
                    superhero.getAppearanceDate().toString()
            );

            GetSuperheroQueryResponse actual = getSuperheroQueryHandler.handle(new GetSuperheroQuery(superheroId));

            assertEquals(expected, actual);
        }

    }

}
