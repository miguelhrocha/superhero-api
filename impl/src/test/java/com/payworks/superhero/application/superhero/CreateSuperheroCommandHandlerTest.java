package com.payworks.superhero.application.superhero;

import com.payworks.superhero.api.v1.AllyTransferObject;
import com.payworks.superhero.domain.model.Ally;
import com.payworks.superhero.domain.model.Skill;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.services.CreateSuperhero;
import com.payworks.superhero.infrastructure.services.ValidateAllies;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import io.github.benas.randombeans.api.Randomizer;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static io.github.benas.randombeans.FieldDefinitionBuilder.field;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class CreateSuperheroCommandHandlerTest {

    private EnhancedRandomBuilder enhancedRandomBuilder;

    @BeforeEach
    public void setUp() {
        enhancedRandomBuilder = EnhancedRandomBuilder
                .aNewEnhancedRandomBuilder()
                .randomize(
                        field().named("publisher").ofType(String.class).inClass(CreateSuperheroCommand.class).get(),
                        (Randomizer<String>) () -> EnhancedRandom.random(PublisherEnum.class).toString()
                )
                .randomize(
                        field().named("firstAppearanceDate").ofType(String.class).inClass(CreateSuperheroCommand.class).get(),
                        (Randomizer<String>) () -> LocalDate.now().toString()
                )
                .randomize(
                        field().named("allies").ofType(List.class).inClass(CreateSuperheroCommand.class).get(),
                        (Randomizer<List<AllyTransferObject>>) () -> new ArrayList<>() {{
                            add(new AllyTransferObject(UUID.randomUUID(), EnhancedRandom.random(String.class)));
                            add(new AllyTransferObject(UUID.randomUUID(), EnhancedRandom.random(String.class)));
                        }}
                );
    }

    @Nested
    @DisplayName("Tests the correct instantiation of the CreateSuperheroCommandHandler")
    class handle {

        @InjectMocks
        private CreateSuperheroCommandHandler createSuperheroCommandHandler;

        @Mock
        private CreateSuperhero createSuperhero;

        @Mock
        private ValidateAllies validateAllies;

        @Mock
        private Supplier<SuperheroId> superheroIdSupplier;

        private SuperheroId superheroId = EnhancedRandom.random(SuperheroId.class);

        private Superhero superhero;

        @BeforeEach
        public void setUp() {
            when(superheroIdSupplier.get()).thenReturn(superheroId);

            superhero = new Superhero(
                    superheroId,
                    EnhancedRandom.random(String.class),
                    EnhancedRandom.random(String.class),
                    EnhancedRandom.random(PublisherEnum.class),
                    EnhancedRandom.randomListOf(5, Skill.class),
                    null,
                    EnhancedRandom.random(LocalDate.class)
            );

            when(createSuperhero.create(superhero))
                    .thenReturn(EnhancedRandom.random(Superhero.class));
        }

        @Test
        @DisplayName("Throws InvalidPublisherException when command request has any value besides DC or MARVEL")
        public void throwsInvalidPublisherExceptionWhenPublisherParamIsNotDcOrMarvel() {
            assertThrows(InvalidPublisherException.class, () ->
                    createSuperheroCommandHandler.handle(EnhancedRandom.random(CreateSuperheroCommand.class)));

            verifyZeroInteractions(validateAllies);
            verifyZeroInteractions(createSuperhero);
        }

        @Test
        @DisplayName("Throws NullPointerException when command request name value its null or blank")
        public void throwsNullPointerExceptionWhenNameIsNullOrBlank() {
            String expectedMessage = "Superhero name cannot be null";

            EnhancedRandom enhancedRandom = enhancedRandomBuilder
                    .exclude(field().named("name").ofType(String.class).inClass(CreateSuperheroCommand.class).get())
                    .build();

            Throwable throwable = assertThrows(NullPointerException.class, () -> createSuperheroCommandHandler.handle(
                    enhancedRandom.nextObject(CreateSuperheroCommand.class)
            ));

            assertEquals(expectedMessage, throwable.getMessage());
        }

        @Test
        @DisplayName("Throws NullPointerException when appearance date is null or blank")
        public void throwsNullPointerExceptionWhenAppearanceDateIsNullOrBlank() {
            String expectedMessage = "Appearance date cannot be null or blank";

            EnhancedRandom enhancedRandom = enhancedRandomBuilder
                    .exclude(field().named("firstAppearanceDate").ofType(String.class).inClass(CreateSuperheroCommand.class).get())
                    .build();

            Throwable throwable = assertThrows(NullPointerException.class, () -> createSuperheroCommandHandler.handle(
                    enhancedRandom.nextObject(CreateSuperheroCommand.class)
            ));

            assertEquals(expectedMessage, throwable.getMessage());
        }

        @ParameterizedTest
        @DisplayName("Throws DateTimeParseException when appearance's date is not in YYYY-MM-DD format (ISO LOCAL DATE)")
        @ValueSource(strings = {"Tuesday, Aug 16, 2016 12:10:56 PM", "2016-08-16T15:23:01Z"})
        public void throwsDateTimeParseExceptionWhenAppearanceDateFormatIsInvalid(String date) {
            EnhancedRandom enhancedRandom = enhancedRandomBuilder
                    .randomize(
                            field().named("firstAppearanceDate").ofType(String.class).inClass(CreateSuperheroCommand.class).get(),
                            (Randomizer<String>) () -> date)
                    .build();

            assertThrows(DateTimeParseException.class, () -> createSuperheroCommandHandler.handle(
                    enhancedRandom.nextObject(CreateSuperheroCommand.class)
            ));
        }

        @Test
        @DisplayName("Calls CreateSuperhero data source interface")
        public void callsCreateSuperhero() {
            CreateSuperheroCommand command = new CreateSuperheroCommand(
                    superhero.getName(),
                    superhero.getPseudonym(),
                    superhero.getPublisher().toString(),
                    superhero.getSkills().stream().map(Skill::getDescription).collect(Collectors.toList()),
                    null,
                    superhero.getAppearanceDate().toString()
            );

            createSuperheroCommandHandler.handle(command);

            verify(createSuperhero).create(superhero);
        }

        @Test
        @DisplayName("Calls ValidateAllies data source interface")
        public void callsValidateAllies() {
            CreateSuperheroCommand command = enhancedRandomBuilder.build().nextObject(CreateSuperheroCommand.class);

            List<Ally> allies = command
                    .getAllies()
                    .stream()
                    .map(a -> new Ally(new SuperheroId(a.getId()), a.getName()))
                    .collect(Collectors.toList());

            when(createSuperhero.create(any(Superhero.class))).thenReturn(EnhancedRandom.random(Superhero.class));
            when(validateAllies.validate(allies)).thenReturn(true);

            createSuperheroCommandHandler.handle(command);

            verify(validateAllies).validate(allies);
        }

        @Test
        @DisplayName("Returns created superhero id and name to the end user")
        public void returnsCorrectCreatedSuperheroValues() {
            CreateSuperheroCommandResponse expected = new CreateSuperheroCommandResponse(
                    superheroId.getIdString(),
                    superhero.getName()
            );

            CreateSuperheroCommand command = new CreateSuperheroCommand(
                    superhero.getName(),
                    superhero.getPseudonym(),
                    superhero.getPublisher().toString(),
                    superhero.getSkills().stream().map(Skill::getDescription).collect(Collectors.toList()),
                    null,
                    superhero.getAppearanceDate().toString()
            );

            when(createSuperhero.create(superhero)).thenReturn(superhero);

            CreateSuperheroCommandResponse actual = createSuperheroCommandHandler.handle(command);

            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Throws InvalidAllyException when ValidateAllies return false")
        public void throwsInvalidAllyExceptionWhenAlliesAreInvalid() {
            CreateSuperheroCommand command = enhancedRandomBuilder.build().nextObject(CreateSuperheroCommand.class);

            when(validateAllies.validate(any())).thenReturn(false);

            assertThrows(InvalidAllyException.class, () -> createSuperheroCommandHandler.handle(command));

            verifyZeroInteractions(createSuperhero);
        }

    }

}
