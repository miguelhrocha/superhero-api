package com.payworks.superhero.application.superhero;

import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.infrastructure.services.GetSuperheroList;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class GetSuperheroListQueryHandlerTest {

    @Nested
    class handle {

        @InjectMocks
        private GetSuperheroListQueryHandler getSuperheroListQueryHandler;

        @Mock
        private GetSuperheroList getSuperheroList;

        private int offset = 0;

        private GetSuperheroListQuery getSuperheroListQuery;

        private List<Superhero> superheroes;

        @BeforeEach
        public void setUp() {
            getSuperheroListQuery = new GetSuperheroListQuery(offset);
            superheroes = EnhancedRandom.randomListOf(3, Superhero.class);
            when(getSuperheroList.getList(offset)).thenReturn(superheroes);
        }

        @Test
        @DisplayName("Verifies that the query handler calls the GetSuperheroListInterface")
        public void callsGetSuperheroList() {
            getSuperheroListQueryHandler.handle(getSuperheroListQuery);

            verify(getSuperheroList).getList(offset);
        }

        @Test
        @DisplayName("Returns correct values from get superhero list method")
        public void returnsCorrectListOfSuperheroes() {
            GetSuperheroListQueryResponse expected = new GetSuperheroListQueryResponse(superheroes);

            GetSuperheroListQueryResponse actual = getSuperheroListQueryHandler.handle(getSuperheroListQuery);

            assertEquals(expected, actual);
        }

    }

}
