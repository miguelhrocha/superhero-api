package com.payworks.superhero.api.v1;

import com.payworks.superhero.application.RequestHandler;
import com.payworks.superhero.application.superhero.*;
import com.payworks.superhero.domain.model.Superhero;
import com.payworks.superhero.domain.model.SuperheroId;
import com.payworks.superhero.infrastructure.mappers.entities.AllyMapper;
import com.payworks.superhero.infrastructure.mappers.entities.SkillMapper;
import io.github.benas.randombeans.EnhancedRandomBuilder;
import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class SuperheroApiImplTest {

    private EnhancedRandom enhancedRandom = EnhancedRandomBuilder.aNewEnhancedRandom();

    private String name = enhancedRandom.nextObject(String.class);
    private String pseudonym = enhancedRandom.nextObject(String.class);
    private String publisher = enhancedRandom.nextObject(String.class);
    private List<String> skills = EnhancedRandom.randomListOf(10, String.class);
    private List<AllyTransferObject> allies = EnhancedRandom.randomListOf(10, AllyTransferObject.class);
    private String appearanceDate = enhancedRandom.nextObject(String.class);

    @Nested
    @DisplayName("Create superhero")
    class createSuperhero {

        @InjectMocks
        private SuperheroApiImpl superheroApi;

        @Mock
        private RequestHandler<CreateSuperheroCommand, CreateSuperheroCommandResponse> createSuperheroCommandHandler;

        private CreateSuperheroRequest createSuperheroRequest = new CreateSuperheroRequest(
                name,
                pseudonym,
                publisher,
                skills,
                allies,
                appearanceDate
        );

        private CreateSuperheroCommand createSuperheroCommand = new CreateSuperheroCommand(
                name,
                pseudonym,
                publisher,
                skills,
                allies,
                appearanceDate
        );

        @BeforeEach
        public void setUp() {
            when(createSuperheroCommandHandler.handle(createSuperheroCommand))
                    .thenReturn(enhancedRandom.nextObject(CreateSuperheroCommandResponse.class));
        }

        @Test
        @DisplayName("verify call to the CreateSuperheroCommandHandler")
        public void callsCreateSuperheroCommandHandler() {
            superheroApi.createSuperhero(createSuperheroRequest);

            verify(createSuperheroCommandHandler).handle(createSuperheroCommand);
        }

        @Test
        @DisplayName("Returns correct API response with superhero ID and superhero name")
        public void returnsSuperheroIdAndName() {
            UUID superheroId = UUID.randomUUID();

            CreateSuperheroCommandResponse commandResponse = new CreateSuperheroCommandResponse(
                    superheroId.toString(),
                    name
            );

            CreateSuperheroResponse expected = new CreateSuperheroResponse(
                    superheroId.toString(),
                    name
            );

            when(createSuperheroCommandHandler.handle(createSuperheroCommand))
                    .thenReturn(commandResponse);

            CreateSuperheroResponse actual = superheroApi.createSuperhero(createSuperheroRequest);

            assertEquals(expected, actual);
        }

    }

    @Nested
    @DisplayName("Gets a superhero")
    class getSuperhero {

        @InjectMocks
        private SuperheroApiImpl superheroApi;

        @Mock
        private RequestHandler<GetSuperheroQuery, GetSuperheroQueryResponse> getSuperheroQueryHandler;

        private SuperheroId superheroId = enhancedRandom.nextObject(SuperheroId.class);

        private GetSuperheroQuery getSuperheroQuery = new GetSuperheroQuery(superheroId);

        private GetSuperheroQueryResponse getSuperheroQueryResponse = new GetSuperheroQueryResponse(
                superheroId.getIdString(),
                name,
                pseudonym,
                publisher,
                skills,
                allies,
                appearanceDate
        );

        @BeforeEach
        public void setUp() {
            when(getSuperheroQueryHandler.handle(getSuperheroQuery))
                    .thenReturn(getSuperheroQueryResponse);
        }

        @Test
        @DisplayName("Calls GetSuperheroQueryHandler cohesion test")
        public void callsGetSuperheroQueryHandler() {
            superheroApi.getSuperhero(superheroId.getIdString());

            verify(getSuperheroQueryHandler).handle(any());
        }

        @Test
        @DisplayName("Returns superhero correct values from query")
        public void returnsExpectedSuperhero() {
            GetSuperheroResponse expected = new GetSuperheroResponse(
                    superheroId.getIdString(),
                    name,
                    pseudonym,
                    publisher,
                    skills,
                    allies,
                    appearanceDate
            );

            when(getSuperheroQueryHandler.handle(getSuperheroQuery))
                    .thenReturn(getSuperheroQueryResponse);

            GetSuperheroResponse actual = superheroApi.getSuperhero(superheroId.getIdString());

            assertEquals(expected, actual);
        }

    }

    @Nested
    class getSuperheroList {

        @InjectMocks
        private SuperheroApiImpl superheroApi;

        @Mock
        private RequestHandler<GetSuperheroListQuery, GetSuperheroListQueryResponse> getSuperheroListQueryHandler;

        @Spy
        private SkillMapper skillMapper;

        @Spy
        private AllyMapper allyMapper;

        private int offset = 0;

        private GetSuperheroListQueryResponse getSuperheroListQueryResponse = EnhancedRandom.random(GetSuperheroListQueryResponse.class);

        private GetSuperheroListQuery getSuperheroListQuery = new GetSuperheroListQuery(offset);

        @BeforeEach
        public void setUp() {
            when(getSuperheroListQueryHandler.handle(getSuperheroListQuery)).thenReturn(getSuperheroListQueryResponse);
        }

        @Test
        @DisplayName("Calls GetSuperheroListQueryHandler cohesion test")
        public void callsGetSuperheroListQueryHandler() {
            superheroApi.getSuperheroList(offset);

            verify(getSuperheroListQueryHandler).handle(getSuperheroListQuery);
        }

        @Test
        @DisplayName("Returns correct list of superheroes")
        public void returnsCorrectListOfSuperheroes() {
            List<Superhero> superheroes = EnhancedRandom.randomListOf(4, Superhero.class);

            List<GetSuperheroResponse> superheroResponses = superheroes
                    .stream()
                    .map(s -> new GetSuperheroResponse(
                            s.getSuperheroId().getIdString(),
                            s.getName(),
                            s.getPseudonym(),
                            s.getPublisher().toString(),
                            skillMapper.mapToCollectionOfTransferObjects(s.getSkills()),
                            allyMapper.mapToCollectionOfTransferObjects(s.getAllies()),
                            s.getAppearanceDate().toString()
                    ))
                    .collect(Collectors.toList());
            GetSuperheroListResponse expected = new GetSuperheroListResponse(superheroResponses);

            GetSuperheroListQueryResponse queryResponse = new GetSuperheroListQueryResponse(superheroes);

            when(getSuperheroListQueryHandler.handle(getSuperheroListQuery)).thenReturn(queryResponse);

            GetSuperheroListResponse actual = superheroApi.getSuperheroList(offset);

            assertEquals(expected, actual);
        }

        @Test
        @DisplayName("Throws IllegalArgumentException if offset is a negative number")
        public void throwsIllegalArgumentExceptionWhenOffsetIsNegative() {
            String expectedMessage = "Offset cannot be less than zero";

            Throwable throwable = assertThrows(
                    IllegalArgumentException.class,
                    () -> superheroApi.getSuperheroList(-1)
            );

            assertEquals(expectedMessage, throwable.getMessage());
        }

    }

}
