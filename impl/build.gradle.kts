plugins {
    java
}

dependencies {
    compile(project(":api"))

    compile("org.apache.commons", "commons-lang3", "3.7")

    compile("com.google.inject", "guice", "4.2.0")

    testCompile("org.junit.jupiter", "junit-jupiter-api", "5.2.0")
    testCompile("org.junit.jupiter", "junit-jupiter-engine", "5.2.0")
    testCompile("org.junit.jupiter","junit-jupiter-params","5.2.0")

    testCompile("org.mockito", "mockito-junit-jupiter", "2.21.0")
    testCompile("org.mockito", "mockito-core", "2.21.0")

    testCompile("io.github.benas", "random-beans", "3.7.0")

}