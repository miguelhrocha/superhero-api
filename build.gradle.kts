plugins {
    java
    id("com.github.johnrengelman.shadow") version "2.0.2"
    id("com.avast.gradle.docker-compose") version "0.8.4"
}

subprojects {

    group = "com.payworks"
    version = "1.0-SNAPSHOT"

    repositories {
        jcenter()
        mavenCentral()
    }

}

dependencies {
    subprojects.forEach {
        archives(it)
    }
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_10
}